require('dotenv').config()
var request = require('request')
var Botkit = require('botkit')

var controller = Botkit.facebookbot({
  access_token: process.env.FB_PAGE_TOKEN,
  verify_token: process.env.FB_VERIFY_TOKEN
})

var bot = controller.spawn({
})

// if you are already using Express, you can use your own server instance...
// see "Use BotKit with an Express web server"
controller.setupWebserver(process.env.port || process.env.PORT, function (err, webserver) {
  controller.createWebhookEndpoints(controller.webserver, bot, function () {
    console.log('This bot is online!!!')
  })
})

// this is triggered when a user clicks the send-to-messenger plugin
controller.on('facebook_optin', function (bot, message) {
  bot.reply(message, 'Welcome to food search!')
})

// user said hello
controller.hears('.*', 'message_received', function (bot, message) {
  console.log('msg received')
  var dataString = '{"q":"' + message.message.text + '"}'
  var options = {
    url: process.env.RASA + '/parse',
    method: 'POST',
    body: dataString
  }
  request(options, (error, response, body) => {
    console.log('rasa request ok.')
    if (!error && response.statusCode === 200) {
      var intent = JSON.parse(body).intent.name
      request(process.env.JSONSERVER, function (error, response, body) {
        console.log('json request ok')
        var items = JSON.parse(body)[intent]
        console.log('items len: ' + items.length)
        if (items) {
          var msgTmp = items[Math.floor(Math.random() * items.length)]
          bot.replyWithTyping(message, msgTmp)
        } else {
          bot.reply(message, 'I can not serve that request.')
        }
      })
    } else {
      console.log('rasa error..', error)
      bot.reply(message, 'Hey, I am unable to understand.')
    }
  })
})
